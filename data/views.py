from django.shortcuts import HttpResponse, render
from collections import Counter
from data.models import *
from operator import attrgetter
import csv
from datetime import timedelta, datetime as dt


def passenger_list(request):
    try:
        with open('passenger.txt') as csvfile:
            read = csv.DictReader(csvfile)
            print(read)
            instances = [
                Passenger(
                    pass_name=row['passname'],
                    pass_email=row['passemail'],
                    pass_dob=row['passdob']
                )
                for row in read
            ]

            data = Passenger.objects.bulk_create(instances)
        return render(request, 'successful.html')
    except:
        return HttpResponse("An Error Occured")


def flight_list(request):
    try:
        with open('flight.txt') as csvfile:
            read = csv.DictReader(csvfile)
            instances = [
                Flight(
                    flight_source=row['flightsource'],
                    flight_dest=row['flightdest'],
                    flight_date=row['flightdate'],
                    flight_seat=row['flightseat'],
                    ticket_cost=row['ticketcost']
                )
                for row in read
            ]

            data = Flight.objects.bulk_create(instances)
        return render(request, 'successful.html')
    except:
        return HttpResponse("An Error Occured")


def booking(request):
    try:
        with open('booking.txt') as csvfile:
            read = csv.DictReader(csvfile)
            for row in read:
                b = row['bookdate']
                a = row['flightid']
                f = Flight.objects.get(id=a)
                data = Booking.objects.create(flight_id=f, book_date=b)
                data.save()
        return render(request, 'successful.html')
    except:
        return HttpResponse("An Error Occured")


def booking_details(request):
    try:
        with open('bookingdetails.txt') as csvfile:
            read = csv.DictReader(csvfile)
            for row in read:
                a = row['bookingid']
                b = row['passid']
                c = Booking.objects.get(id=a)
                d = Passenger.objects.get(id=b)
                data = BookingDetails.objects.create(booking_id=c, pass_id=d)
                data.save()
            return render(request, 'successful.html')

    except:
        return HttpResponse("An Error Occured")


def delete(request):
    data = Passenger.objects.all()
    data.delete()
    return HttpResponse("deleted")


# 1. Display passenger name who has a 'e' as second letter in their name
def pass_name():
    a = Passenger.objects.values_list('pass_name', flat='True')
    for i in a:
        if (i[1]) == 'e':
            print("passenger name :", i)


pass_name()


# 2. Display the name of the youngest passenger.
def young_passenger():
    a = Passenger.objects.values_list('pass_dob', flat=True)
    b = Passenger.objects.filter(pass_dob=max(a)).values_list('pass_name', flat=True)
    print(b)


# young_passenger()

# 3. Display the name of the passenger, date of birth and age.
def pass_details():
    b = Passenger.objects.values_list('pass_dob', flat=True)
    for i in b:
        dob = dt.today().strftime("%Y")
        c = dt.strftime(i, "%Y")
        z = int(dob) - int(c)
        x = Passenger.objects.get(pass_dob=i).pass_name
        print(x, i, z)


# pass_details()

# 4. Display the number of flights leaving Kolkata.
def num_kol():
    a = Flight.objects.values_list('flight_source', flat=True)
    c = Counter(a)
    b = [e for d, e in c.items() if d == 'kol']
    return "num of flights from kolkata :", b


# print(num_kol())

# 5. Display the name of city where the number of flights leaving and reaching is the same
def city_names():
    a = Flight.objects.values_list('flight_source', flat=True)
    b = Flight.objects.values_list('flight_dest', flat=True)
    c = Counter(a)
    d = Counter(b)
    for i, j in c.items():
        for k, l in d.items():
            if i == k and j == l:
                print(i)


# city_names()

# 6. Display the name of the city which has flight source but no destination.
def no_dest():
    a = Flight.objects.values_list('flight_source', flat=True)
    b = Flight.objects.values_list('flight_dest', flat=True)
    c = [x for x in a if x not in b]
    print(c)


# no_dest()

# 7. Display the dates on which flight 1 and 4 is flying.
def flight_date():
    a = Flight.objects.filter(id__in=[1, 4]).values_list('flight_date', flat=True)
    for j in a:
        print(dt.strftime(j, "%Y"'/'"%m"'/'"%d"))


# flight_date()


# 8. Display the number of passenger in each flight. Use column alias “PassCount”.
def num_pass():
    a = Booking.objects.values_list('flight_id', flat=True)
    b = Counter(a)
    for i, j in b.items():
        print('flight id :', i, 'pass_count:', j)


# num_pass()

# 9. Display the name and date of birth of passengers who are senior citizen (age>=60).
def senior_citizen():
    b = Passenger.objects.values_list('pass_dob', flat=True)
    for i in b:
        dob = dt.today().strftime("%Y")
        c = dt.strftime(i, "%Y")
        z = int(dob) - int(c)
        if z >= 60:
            x = Passenger.objects.get(pass_dob=i).pass_name
            print('pass_name:', x, 'age', i)


# senior_citizen()

# 10. Display the booking id having the highest number of passengers.
def max_booking():
    a = BookingDetails.objects.values_list('booking_id', flat=True)
    z = Counter(a)
    b = max(z.values())
    for i, j in z.items():
        if j == b:
            print('number of times buked : ', j, 'booking id : ', i)


# max_booking()

# 11. Display the booking id (ticket) and the total cost for the booking. Use column alias “Total Fare”.
def total_fare():
    a = Booking.objects.values_list('id', 'flight_id__ticket_cost')
    for i, j in a:
        print('booking id', i, 'total fare', int(j))


# total_fare()

# 12. Display the booking id (ticket) and the total cost for the booking.
# Use column alias “Total Fare”. Consider giving a rebate of 50% to senior citizen (age>=60).
def senior_citizen_fare():
    b = Passenger.objects.values_list('pass_dob', flat=True)
    for i in b:
        dob = dt.today().strftime("%Y")
        c = dt.strftime(i, "%Y")
        z = int(dob) - int(c)
        a = BookingDetails.objects.filter(pass_id__pass_dob=i).values_list('booking_id_id', flat=True)
        for j in a:
            d = Flight.objects.filter(booking__bookingdetails__pass_id=j).values_list('ticket_cost', flat=True)
            f = list(d)
            if z >= 60:
                print(j, int(f[0] / 2))
            else:
                print(j, int(f[0]))


# senior_citizen_fare()

# 13. Display the city receiving the maximum number of flights.
def max_dest():
    a = Flight.objects.values_list('flight_dest', flat=True)
    c = Counter(a)
    b = max(c, key=c.get)
    print('highest number of receiving flights:', b)


# max_dest()

# 14. Display the passenger’s name having more than 1 booking.
def num_of_booking():
    a = BookingDetails.objects.values_list('pass_id__pass_name', flat=True)
    c = Counter(a)
    b = [i for i, j in c.items() if j > 1]
    print(b)


# num_of_booking()

# 15. Display flight_id with no of booking.
def flight_booking():
    a = BookingDetails.objects.values_list('booking_id__flight_id', flat=True)
    b = Counter(a)
    for i, j in b.items():
        print('flight id:', i, 'num of booking:', j)


# flight_booking()

# 16. Display the passenger (name only) who booked ticket on the day of flight for flight no 1.
def pass_name_id():
    a = BookingDetails.objects.filter(booking_id__flight_id=1).values_list('booking_id__book_date', flat=True)
    b = Flight.objects.filter(id=1).values_list('flight_date', flat=True)
    c = [dt.strftime(i, "%Y-%m-%d") for i in b]
    for i in a:
        if str(i) == c[0]:
            d = Passenger.objects.filter(bookingdetails__booking_id__book_date=i).values_list('pass_name', flat=True)
    print(list(d))


# pass_name_id()


# 17. Display flights having the same source and destination.
def source_dest():
    a = Flight.objects.values_list('id', flat=True)
    for i in a:
        b = Flight.objects.filter(id=i).values_list('flight_source', 'flight_dest')
        for j, k in b:
            if j == k:
                print(j)


# source_dest()

# 18. Display the record in the following format. Column alias “Booking Summary”
# -- Hints:  “ Ticket No:1 Flight id: 1 Total Passengers :3 Total Fare:6000”
# -- “Ticket No:2 Flight id: 3 Total Passengers :1 Total Fare :2500”

def total_details():
    c = BookingDetails.objects.values_list('booking_id__flight_id_id', flat=True)
    a = Counter(c)
    for i, j in a.items():
        b = Flight.objects.filter(id=i).values_list('ticket_cost', flat=True)
        for k in b:
            print('flight id :', i, 'no.of booking :', j, 'total fare :', int(k * j))


# total_details()

# # 19.Flight No: 2 have been delayed for 4 hrs due to fog. Display flight id ,
# -- flight date and a new column “flight new date”, which displays the new timing.

def new_time():
    a = Flight.objects.get(id=2).flight_date
    i = dt.strftime(a, '%d:%m:%Y:%H:%M:%S')
    the_time = dt.strptime(i, '%d:%m:%Y:%H:%M:%S')
    the_new_time = the_time + timedelta(hours=4)
    print("new time :", the_new_time)


# new_time()


# 20. Display passenger name , date of birth sorted by the month of birth .(Jan ? Dec).

def sort_month():
    b = Passenger.objects.values_list('pass_dob', flat=True)
    a = sorted(b, key=attrgetter('month'))
    for i in a:
        c = Passenger.objects.filter(pass_dob=i).values_list('pass_name', flat=True)
        print(i, c)
# sort_month()
