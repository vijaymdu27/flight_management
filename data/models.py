from django.db import models


class Passenger(models.Model):
    pass_name = models.CharField(max_length=20)
    pass_email = models.EmailField(max_length=20)
    pass_dob = models.DateField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id)


class Flight(models.Model):
    flight_source = models.CharField(max_length=20)
    flight_dest = models.CharField(max_length=20)
    flight_date = models.DateTimeField(null=True)
    flight_seat = models.IntegerField(null=True)
    ticket_cost = models.FloatField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name_plural = 'Flight'


class Booking(models.Model):
    flight_id = models.ForeignKey(Flight, on_delete=models.CASCADE)
    book_date = models.DateField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name_plural = 'Booking'


class BookingDetails(models.Model):
    booking_id = models.ForeignKey(Booking, on_delete=models.CASCADE)
    pass_id = models.ForeignKey(Passenger, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.booking_id)

    class Meta:
        verbose_name_plural = 'Booking_details'
