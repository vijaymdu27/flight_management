from django.contrib import admin
from data.models import Passenger,Flight,Booking,BookingDetails
# Register your models here.

admin.site.register(Passenger)
admin.site.register(Flight)
admin.site.register(Booking)
admin.site.register(BookingDetails)
