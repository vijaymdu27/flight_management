from django.urls import path
from . import views

urlpatterns = [
    path('passenger/', views.passenger_list, name='index'),
    path('flight/', views.flight_list, name='flight'),
    path('booking/', views.booking, name='booking'),
    path('bdetails/', views.booking_details, name='bdetails'),
    path('delete/', views.delete, name='delete')
]
