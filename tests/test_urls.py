from django.test import TestCase
from django.urls import reverse, resolve
from data.views import passenger_list, flight_list, booking_details, booking, delete


class TestPassenger(TestCase):

    def test_index(self):
        dx = reverse('index')
        self.assertEqual(resolve(dx).func, passenger_list)
        print("index function passed")

    def test_flight(self):
        b = reverse('flight')
        self.assertEqual(resolve(b).func, flight_list)
        print("flight function passed")

    def test_booking(self):
        c = reverse('booking')
        self.assertEqual(resolve(c).func, booking)
        print("booking function passed")

    def test_bdetails(self):
        d = reverse('bdetails')
        self.assertEqual(resolve(d).func, booking_details)
        print("booking_details function passed")

    def test_delete(self):
        e = reverse('delete')
        self.assertEqual(resolve(e).func, delete)
        print("delete function passed")
