from django.test import TestCase, Client
from django.urls import reverse
from data.views import *


class TestViews(TestCase):

    def setUp(self):
        self.Client = Client()

    def test_pass_list(self):
        response = self.Client.get(reverse('index'))
        self.assertTemplateUsed(response, 'successful.html')
        self.assertEqual(response.status_code, 200)
        print("passenger_list function and templates successfully passing")

    def test_flight_list(self):
        response = self.Client.get(reverse('flight'))
        self.assertEquals(response.status_code, 200)
        print("passenger_list function and templates successfully passing")

    def test_booking(self):
        response = self.Client.get(reverse('booking'))
        self.assertEquals(response.status_code, 200)
        print("booking function and templates successfully passing")

    def test_booking_details(self):
        response = self.Client.get(reverse('bdetails'))
        self.assertEquals(response.status_code, 200)
        print("booking details function and templates successfully passing")

    def test_num_kol(self):
        self.response = num_kol()
        self.assertEqual(self.response, ('num of flights from kolkata :', []))
        print("success")
